import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import styles from "../styles/Home.module.css";

const Home: NextPage = () => {
  const openMediaDevices = async (
    constraints: MediaStreamConstraints | undefined
  ) => {
    return await navigator.mediaDevices.getUserMedia(constraints);
  };
  
  const [sChannel, setSChannel] = useState<BroadcastChannel>();
  const [pConnection, setPConnection] = useState<RTCPeerConnection>();
  const [localStream, setLocalStream] = useState<MediaStream>();
  const [screenMediaStream, setScreenMediaStream] = useState<MediaStream>();
  useEffect(() => {
    const signalingChannel = new BroadcastChannel("webrtc");
    setSChannel(signalingChannel);
    const peerConnection = new RTCPeerConnection({'iceServers': [{'urls': 'stun:stun.l.google.com:19302'}]});
    setPConnection(peerConnection);
  }, []);
  const remoteVideo = useRef<HTMLVideoElement>();

  async function handleOffer(offer: any) {

    await createPeerConnection();
    console.log(pConnection,"fghj");
    
    await pConnection?.setRemoteDescription(offer);

    const answer = await pConnection?.createAnswer();
    sChannel?.postMessage({ type: "answer", sdp: answer.sdp });
    await pConnection?.setLocalDescription(answer);
  }
  function createPeerConnection() {
    if (!pConnection) {
      const peerConnection = new RTCPeerConnection({'iceServers': [{'urls': 'stun:stun.l.google.com:19302'}]});
      setPConnection(peerConnection);
    }

    if (pConnection) {
      pConnection.onicecandidate = (e) => {
        const message: any = {
          type: "candidate",
          candidate: null,
        };
        if (e.candidate) {
          message.candidate = e.candidate.candidate;
          message.sdpMid = e.candidate.sdpMid;
          message.sdpMLineIndex = e.candidate.sdpMLineIndex;
        }
        sChannel?.postMessage(message);
      };
      pConnection.ontrack = (e) => {
        if (remoteVideo.current) {
          remoteVideo.current.srcObject = e.streams[0];
        }
      };
      localStream
        ?.getTracks()
        .forEach((track) => pConnection.addTrack(track, localStream));
    }
  }

  const makeCall = async () => {
    await createPeerConnection();

    if (pConnection) {
      const offer = await pConnection.createOffer();
      console.log(offer, "offer");
      sChannel?.postMessage({ type: "offer", sdp: offer.sdp });
      

      await pConnection.setLocalDescription(offer);
    }
  };

  async function handleCandidate(candidate: { candidate: any }) {
    if (!pConnection) {
      console.error("no peerconnection");
      return;
    }
    if (!candidate.candidate) {
      await pConnection.addIceCandidate(undefined);
    } else {
      await pConnection.addIceCandidate(candidate);
    }
  }

  const answerCall = async (answer: RTCSessionDescriptionInit) => {
    if (!pConnection) {
      console.error("no peerconnection");
      return;
    }
    await pConnection.setRemoteDescription(answer);
  };

  const hangup = async () => {
    if (pConnection) {
      pConnection.close();
    }
    setPConnection(undefined);
    localStream?.getTracks().forEach((el) => el.stop());
    if (myVideo.current) {
      myVideo.current.srcObject = null;
    }
    setLocalStream(undefined);
    screenMediaStream?.getTracks().forEach((el) => el.stop());
    setScreenMediaStream(undefined);
  };

  const screenMedia = async (
    constraints: DisplayMediaStreamConstraints | undefined
  ) => {
    return await navigator.mediaDevices.getDisplayMedia(constraints);
  };

  const getConnectedDevices = async (type: string) => {
    const devices = await navigator.mediaDevices.enumerateDevices();
    return devices.filter((device) => device.kind === type);
  };
  const [audioDevices, setAudioDevices] = useState<MediaDeviceInfo[]>([]);
  const [videoDevices, setVideoDevices] = useState<MediaDeviceInfo[]>([]);
  const myVideo = useRef<HTMLVideoElement>();
  const screenShareRef = useRef<HTMLVideoElement>();
  useEffect(() => {
    (async () => {
      // const mediaStream = await screenMedia({});
      setScreenMediaStream(undefined);
      // if (screenShareRef.current) {
      //   screenShareRef.current.srcObject = mediaStream;
      // }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      try {
        const d = await openMediaDevices({ video: true, audio: true });

        setLocalStream(d);

        if (myVideo.current) {
          myVideo.current.srcObject = d;
        }

        const cams = await getConnectedDevices("videoinput");
        const aud = await getConnectedDevices("audioinput");
        setVideoDevices(cams);
        setAudioDevices(aud);
      } catch (error: any) {
        console.log(error.message);
      }
    })();
  }, []);

  useEffect(() => {
    navigator.mediaDevices.addEventListener("devicechange", async (event) => {
      const cams = await getConnectedDevices("videoinput");
      const aud = await getConnectedDevices("audioinput");
      setVideoDevices(cams);
      setAudioDevices(aud);
    });
  }, []);
useEffect(() => {
  if (sChannel) {
    sChannel.onmessage = (e) => {
      console.log(e, "messagae");

      switch (e.data.type) {
        case "offer":
          handleOffer(e.data);
          break;
        case "answer":
          answerCall(e.data);
          break;
        case "candidate":
          handleCandidate(e.data);
          break;
        default:
          console.log("error", e);
          return;
      }
    };
  }
}, [answerCall, handleCandidate, handleOffer, sChannel])

useEffect(() => {
  
  return () => {
    hangup()
  }
}, [])



  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>
        Select Camera
        <br />
        <select>
          {videoDevices.map((el) => (
            <option key={el.deviceId}>{el.label}</option>
          ))}
        </select>
      </div>
      <p>
        Select Microphone
        <br />
        <select>
          {audioDevices.map((el) => (
            <option key={el.deviceId}>{el.label}</option>
          ))}
        </select>
      </p>
      
      <br />
      <br />
      <video playsInline autoPlay muted ref={myVideo} height={300} style={{marginRight:"10px"}} />
      <video playsInline autoPlay muted ref={remoteVideo} height={300} />
      {/* <main className={styles.main}>
        <video playsInline autoPlay muted ref={screenShareRef} height={600} />
      </main> */}
     
      <button onClick={makeCall}>Call</button>
      {/* <button>Answer</button> */}
      <button onClick={hangup}>Hang Up</button>
      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{" "}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  );
};

export default Home;
